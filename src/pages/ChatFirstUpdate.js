import { useRef, useState } from 'react'
import { Redirect, useHistory } from 'react-router-dom'
import { useAuth } from '../contexts/AuthContext'
import man from '../assets/man.svg'
import woman from '../assets/woman.svg'

export const ChatFirstUpdate = ({ history }) => {

  const [gender, setGender] = useState('')
  const [code, setCode] = useState({})
  const [error, setError] = useState('')
  const [load, setLoad] = useState(false)

  const nameFirstRef = useRef()
  const nameLastRef = useRef()

  const hist = useHistory()
  const { updateProfile, user, code: fetchedCode } = useAuth()

  const submit = async (e) => {
    e.preventDefault()
    if (gender === '') return setError('Please select your gender!')
    if (nameFirstRef.current.value === '') return setError('Please enter your first name!')
    if (nameLastRef.current.value === '') return setError('Please enter your last name!')

    const codeArray = []
    for (const key in code) {
      codeArray.push(code[key])
    }

    let partnerCode = codeArray.length === 6 ? codeArray.join('').toUpperCase().toString() : ''
    if (!(partnerCode in fetchedCode) || fetchedCode[partnerCode] === true) partnerCode = ''

    try {
      setError('')
      setLoad(true)
      const { uid } = user
      const data = { nameFirst: nameFirstRef.current.value, nameLast: nameLastRef.current.value, gender: gender, partnerCode: partnerCode }
      await updateProfile(data, uid)
      hist.push('/chat')
    } catch (error) {
      setLoad(false)
      setError('Something went wrong!')
    }
  }

  const errorMessage = () => error && <div className="border text-center py-2 px-2 rounded-full bg-red-600 opacity-50 text-sm text-gray-50 my-3">{error}</div>

  const loadBtn = () => {
    if (!load) return <button type='submit' className='btn'>Continue</button>
    if (load) return <button type='submit' className='btn flex flex-row items-center justify-center' disabled>continue <i className="fas fa-circle-notch ml-3 animate-spin"></i></button>
  }

  const numberInput = (e) => {
    setCode({ ...code, [e.target.id]: e.target.value })
  }

  const renderForm = () => {
    return (
      <form onSubmit={submit} className=''>
        <div className="flex flex-col my-7 ">
          <input type="text" id='text' ref={nameFirstRef} placeholder='Enter your first name: John' className='form-input' onChange={() => setError(false)} />
        </div>
        <div className="flex flex-col my-7">
          <input type="text" id='text' ref={nameLastRef} placeholder='Enter your last name: Doe' className='form-input' onChange={() => setError(false)} />
        </div>
        <div className="flex flex-col my-2">
          <label htmlFor="" className='form-label text-center'>Enter Your Partner Code (if available)</label>
          <div className="flex flex-row mt-2">
            <input type="text" maxLength='1' id='one' className='code-input' onChange={numberInput} />
            <input type="text" maxLength='1' id='two' className='code-input' onChange={numberInput} />
            <input type="text" maxLength='1' id='three' className='code-input' onChange={numberInput} />
            <input type="text" maxLength='1' id='four' className='code-input' onChange={numberInput} />
            <input type="text" maxLength='1' id='five' className='code-input' onChange={numberInput} />
            <input type="text" maxLength='1' id='six' className='code-input' onChange={numberInput} />
          </div>
        </div>

        {loadBtn()}
      </form>
    )
  }

  const renderGender = () => {
    return (
      <>
        <div className="flex justify-center items-center mb-8 ">
          <embed type="image/svg+xml" src={gender === 'female' ? woman : man} className='w-32' />
        </div>
        <div className="flex flex-row justify-evenly uppercase">
          <div className={`cursor-pointer py-2 ${gender === 'male' && 'text-green-700 border-b-2 border-green-700'}`} onClick={() => setGender('male')} >Man</div>
          <div className={`cursor-pointer py-2 ${gender === 'female' && 'text-green-700 border-b-2 border-green-700'}`} onClick={() => setGender('female')} >Woman</div>
        </div>
      </>
    )
  }

  if (history.location.state !== 'newUser') return <Redirect to='/chat' />

  return (
    <div className=' w-4/5 sm:w-3/5 md:w-2/5 lg:w-2/6'>
      {renderGender()}
      <div className="px-2 pb-5 pt-0">
        {errorMessage()}
        {renderForm()}
      </div>
    </div>
  )
}