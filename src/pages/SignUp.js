import { useEffect, useRef, useState } from 'react'
import { Link, useHistory } from 'react-router-dom'
import { useAuth } from '../contexts/AuthContext'
import couplechatlogo from '../assets/couplechat-logo.svg'
import FB, { AUTH, DB } from '../configs/firebase'

export const SignUp = () => {

  const [error, setError] = useState('')
  const [agree, setAgree] = useState(false)
  const [load, setLoad] = useState(false)

  const emailRef = useRef()
  const passwordRef = useRef()
  const confirmPasswordRef = useRef()

  const history = useHistory()
  const { signup } = useAuth()


  const submit = async (e) => {
    e.preventDefault()
    if (emailRef.current.value === '') return setError('Please enter your email!')
    if (passwordRef.current.value === '') return setError('Please enter your password!')
    if (confirmPasswordRef.current.value !== passwordRef.current.value) return setError('The password must be matched!')
    if (!agree) return setError('Please agree the term and agreement to continue')

    try {
      setError('')
      setLoad(true)
      await signup(emailRef.current.value, passwordRef.current.value)
      history.push({ pathname: '/chat-first-update', state: 'newUser' })
    } catch (error) {
      setLoad(false)
      return setError(error.message)
    }
  }

  const errorMessage = () => error && <div className="border text-center py-2 px-2 rounded-full bg-red-600 opacity-50 text-sm text-gray-50 my-3">{error}</div>

  const loadBtn = () => {
    if (!load) return <button type='submit' className='btn'>sign up</button>
    if (load) return <button type='submit' className='btn flex flex-row items-center justify-center' disabled>signing up <i className="fas fa-circle-notch ml-3 animate-spin"></i></button>
  }

  const agreement = () => {
    setAgree(!agree)
    setError(false)
  }

  const form = () => {
    return (
      <form onSubmit={submit} className=''>
        <div className="flex flex-col my-7 ">
          <input type="email" id='email' ref={emailRef} placeholder='johndoe@example.com' className='form-input' onChange={() => setError(false)} />
        </div>
        <div className="flex flex-col my-7 ">
          <input type="password" id='password' ref={passwordRef} placeholder='Your password' className='form-input' onChange={() => setError(false)} />
        </div>
        <div className="flex flex-col my-7 ">
          <input type="password" id='confirmPassword' ref={confirmPasswordRef} placeholder='Reenter your password' className='form-input' onChange={() => setError(false)} />
        </div>
        <div className="term-agreement">
          <i className={`${!agree ? 'far' : 'fas'} fa-circle text-green-500 cursor-pointer`} onClick={agreement}></i><p className='ml-2'>I agree to the <Link className='text-green-500' to='#'>Terms of use</Link> and <Link to='#' className='text-green-500'>Privacy Policy</Link></p>
        </div>
        {loadBtn()}
      </form>
    )
  }

  return (
    <div className=' w-4/5 sm:w-3/5 md:w-2/5 lg:w-2/6'>
      <div className="flex justify-center items-center mb-8">
        <embed type="image/svg+xml" src={couplechatlogo} className='w-32' />
      </div>
      <div className="auth-title">
        <h3>Welcome to CoupleChat!</h3>
        <p>Please sign up as a member.</p>
      </div>
      <div className="px-2 pb-5 pt-0">
        {errorMessage()}
        {form()}
      </div>
      <div className="flex flex-row justify-between px-2 text-gray-500 text-sm mb-3">
        <Link to='/signin' className='hover:text-green-500'>Sign In</Link>
        <Link to='/forgot-password' className='hover:text-green-500'>Forgot Password?</Link>
      </div>
    </div>
  )
}