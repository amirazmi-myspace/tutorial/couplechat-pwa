import React, { useEffect, useState } from 'react'
import { AUTH, DB } from '../configs/firebase'
import { useAuth } from '../contexts/AuthContext'
import heartbreak from '../assets/heartbreak.svg'

export const Chat = () => {

  const [load, setLoad] = useState(false)
  const [inputCode, setInputCode] = useState({})
  const [partnerData, setPartnerData] = useState({})

  const { currentUser, updateUserData, code, allUser } = useAuth()
  const { uid } = AUTH.currentUser

  useEffect(() => {
    fetchUser()
  }, [])

  const fetchUser = async () => {
    const DB_SNAP = await DB.ref(`/users/${uid}`).once('value')
    if (DB_SNAP.exists()) {
      await updateUserData(DB_SNAP.val())
    }
    if (!DB_SNAP.exists()) {
      console.log('no data')
    }
  }
  const fetchPartner = async (selectedUser) => {
    const DB_SNAP = await DB.ref(`/users/${selectedUser}`).once('value')
    if (DB_SNAP.exists()) {
      await setPartnerData(DB_SNAP.val())
    }
    if (!DB_SNAP.exists()) {
      console.log('no data')
    }
  }


  const selfCode = uid.substring(uid.length - 6).toUpperCase()
  const selfCodeArray = (selfCode in code) && selfCode.split('')

  const numberInput = (e) => setInputCode({ ...inputCode, [e.target.id]: e.target.value })

  const loadBtn = () => {
    if (!load) return <button type='submit' className='btn'>Continue</button>
    if (load) return <button type='submit' className='btn flex flex-row items-center justify-center' disabled>continue <i className="fas fa-circle-notch ml-3 animate-spin"></i></button>
  }

  const submit = async (e) => {
    e.preventDefault()
    const codeArray = []
    for (const key in inputCode) {
      codeArray.push(inputCode[key])
    }
    let partnerCode = codeArray.length === 6 ? codeArray.join('').toUpperCase().toString() : ''
    const selectedUser = Object.keys(allUser).filter(user => user.substring(user.length - 6).toUpperCase() === partnerCode).toString()

    setLoad(true)
    DB.ref(`/users/${uid}/partnerCode`).set(partnerCode)
    DB.ref(`/users/${selectedUser}/partnerCode`).set(partnerCode)
    DB.ref(`/partnerCode/${partnerCode}`).set(true)
    fetchPartner(selectedUser)
    fetchUser()
    setPartnerData()
    setLoad(false)
  }

  const renderNoPartner = () => {
    return (
      <div className="flex flex-col">
        <div className="my-7">
          <h2 className='text-5xl text-center'>You're Alone...</h2>
        </div>
        <div className="flex justify-center items-center mb-8">
          <embed type="image/svg+xml" src={heartbreak} className='w-32' />
        </div>
        <div className="px-2 text-center">
          <p className='mb-2'>It seems you do not has a partner yet.</p>
          <p className='mb-2'>Maybe the code you enter not valid, or you not inform your partner about this app yet.</p>
          <p className='mb-2'>So, please pass the Partner Code below so that you can enjoy chatting with your beloved privately and happily.</p>
        </div>
        <div className="flex flex-col my-2">
          <div className="flex flex-row mt-2 text-center">
            <div type="text" maxLength='1' id='one' className='w-12 h-14 m-auto text-4xl' >{selfCodeArray[0]}</div>
            <div type="text" maxLength='1' id='two' className='w-12 h-14 m-auto text-4xl' >{selfCodeArray[1]}</div>
            <div type="text" maxLength='1' id='three' className='w-12 h-14 m-auto text-4xl' >{selfCodeArray[2]}</div>
            <div type="text" maxLength='1' id='four' className='w-12 h-14 m-auto text-4xl' >{selfCodeArray[3]}</div>
            <div type="text" maxLength='1' id='five' className='w-12 h-14 m-auto text-4xl' >{selfCodeArray[4]}</div>
            <div type="text" maxLength='1' id='six' className='w-12 h-14 m-auto text-4xl' >{selfCodeArray[5]}</div>
          </div>
        </div>
        <p className='text-2xl text-center text-pink-400'>OR</p>
        <div className="px-2 text-center mt-5">
          <p>Enter the Partner Code that you've obtained from your partner</p>
        </div>
        <form onSubmit={submit}>
          <div className="flex flex-row mt-4">
            <input type="text" maxLength='1' id='one' className='code-input' onChange={numberInput} />
            <input type="text" maxLength='1' id='two' className='code-input' onChange={numberInput} />
            <input type="text" maxLength='1' id='three' className='code-input' onChange={numberInput} />
            <input type="text" maxLength='1' id='four' className='code-input' onChange={numberInput} />
            <input type="text" maxLength='1' id='five' className='code-input' onChange={numberInput} />
            <input type="text" maxLength='1' id='six' className='code-input' onChange={numberInput} />
          </div>
          {loadBtn()}
        </form>
      </div>
    )
  }

  const renderChatArea = () => {
    return (
      <div className="text-xl">
        This is chat area
        <p>Your Name: {currentUser.nameFirst} {currentUser.nameLast}</p>
        <p>Partner Name: {partnerData.nameFirst} {partnerData.nameLast}</p>
      </div>
    )
  }

  return (
    <div className=' w-4/5 sm:w-3/5 md:w-2/5 lg:w-2/6'>
      {currentUser.partnerCode === '' ? renderNoPartner() : renderChatArea()}
    </div>
  )
}
