import { createContext, useContext, useEffect, useState } from "react"
import FB, { AUTH, DB } from "../configs/firebase"

const AuthContext = createContext()
export const useAuth = () => useContext(AuthContext)

export const ProvideAuth = ({ children }) => {
  const [user, setUser] = useState(false)
  const [allUser, setAllUser] = useState(false)
  const [currentUser, setCurrentUser] = useState({})
  const [code, setCode] = useState({})
  const [load, setLoad] = useState(true)

  const signup = async (emailInput, passwordInput) => {
    const { user } = await AUTH.createUserWithEmailAndPassword(emailInput, passwordInput)
    const data = { uid: user.uid, email: user.email, registeredAt: FB.database.ServerValue.TIMESTAMP }
    const start = user.uid.length - 6
    const partnerCode = user.uid.substring(start).toUpperCase()
    await DB.ref(`/partnerCode/${partnerCode}`).set(false)
    return DB.ref(`/users/${user.uid}`).update(data)
  }
  const signin = (email, password) => AUTH.signInWithEmailAndPassword(email, password)
  const logout = () => AUTH.signOut()
  const resetPassword = (email) => AUTH.sendPasswordResetEmail(email)
  const updateEmail = (email) => user.updateEmail(email)
  const updatePassword = (password) => user.updatePassword(password)

  const updateProfile = (data, uid) => {
    return DB.ref(`/users/${uid}`).update(data)
  }

  const fetchPartnerCodes = async () => {
    const DB_SNAP = await DB.ref('/partnerCode').once('value')
    if (DB_SNAP.exists()) {
      const DB_VAL = DB_SNAP.val()
      return setCode(DB_VAL)
    }
    if (!DB_SNAP.exists) {
      return setCode({})
    }
  }

  const fetchAllUsers = async () => {
    const DB_SNAP = await DB.ref('/users').once('value')
    if (DB_SNAP.exists()) {
      const DB_VAL = DB_SNAP.val()
      return setAllUser(DB_VAL)
    }
    if (!DB_SNAP.exists) {
      return setAllUser({})
    }
  }

  const updateUserData = (data) => setCurrentUser(data)

  useEffect(() => {
    fetchPartnerCodes()
    fetchAllUsers()
    const unsubsribe = AUTH.onAuthStateChanged(user => {
      setUser(user)
      setLoad(false)
    })
    return unsubsribe
  }, [])

  const value = { user, signup, signin, logout, resetPassword, updateEmail, updatePassword, updateProfile, fetchPartnerCodes, code, updateUserData, currentUser, allUser }

  return <AuthContext.Provider value={value}>{!load && children}</AuthContext.Provider>
}
