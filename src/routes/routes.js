import { Chat } from '../pages/Chat'
import { ChatFirstUpdate } from '../pages/ChatFirstUpdate'
import { ForgotPassword } from '../pages/ForgotPassword'
import { SignIn } from '../pages/SignIn'
import { SignUp } from '../pages/SignUp'

export const routes = [
  { name: 'sign up', path: '/signup', component: SignUp, private: false },
  { name: 'sign in', path: '/signin', component: SignIn, private: false },
  { name: 'forgot password', path: '/forgot-password', component: ForgotPassword, private: false },

  { name: 'chat first update', path: '/chat-first-update', component: ChatFirstUpdate, private: true },
  { name: 'chat', path: '/chat', component: Chat, private: true }
]
