import { createContext, useContext } from "react"
import { DB } from "../configs/firebase"


const DatabaseContext = createContext()
export const useDatabase = () => useContext(DatabaseContext)

export const ProvideDatabase = ({ children }) => {

  const updateUser = () => DB.ref()

  const value = {}

  return (<DatabaseContext.Provider value={value}>{children}</DatabaseContext.Provider>
  )
}
