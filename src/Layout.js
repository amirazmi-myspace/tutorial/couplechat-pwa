import { Navbar } from "./components/Navbar"

export const LayoutAuth = ({ children, ...rest }) => {
  return (
    <div className='h-screen flex flex-col bg-gray-50'>
      {/* <Navbar {...rest} /> */}
      <div className='flex flex-col justify-center items-center py-auto my-auto text-gray-700'>
        {children}
      </div>
    </div>
  )
}
export const LayoutChat = ({ children, ...rest }) => {
  return (
    <div className='h-screen flex flex-col bg-gray-50'>
      {/* <Navbar {...rest} /> */}
      <div className='flex flex-col justify-center items-center py-auto my-auto text-gray-700'>
        {children}
      </div>
    </div>
  )
}
