import { Redirect, Route } from "react-router-dom"
import { useAuth } from "../contexts/AuthContext"
import { LayoutChat } from "../Layout"

export const PrivateRoute = ({ component: Component, ...rest }) => {
  const { user } = useAuth()
  return <Route {...rest} render={props => user ? <LayoutChat><Component {...props} /></LayoutChat> : <Redirect to='/signin' />} />
}
